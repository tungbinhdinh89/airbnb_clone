// import axiosm from "axios";
import api from "./axios.config";

export const getHomeList = () => {
  return api
    .get("https://airbnb.cybersoft.edu.vn/api/rooms?limit=32")
    .then(x => x.data)
    .catch(err => {
      console.log(err);
    });
};
