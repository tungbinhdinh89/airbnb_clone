import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
var Carousel = require("react-responsive-carousel").Carousel;

export default function HomeSlider() {
  const data = [
    {
      img: "https://a0.muscache.com/pictures/c5a4f6fc-c92c-4ae8-87dd-57f1ff1b89a6.jpg",
      text: "Thật ấn tượng",
    },
    {
      img: "https://a0.muscache.com/pictures/c0a24c04-ce1f-490c-833f-987613930eca.jpg",
      text: "Công viên quốc gia",
    },
    {
      img: "https://a0.muscache.com/pictures/3fb523a0-b622-4368-8142-b5e03df7549b.jpg",
      text: "Hồ bơi tuyệt vời",
    },
    {
      img: "https://a0.muscache.com/pictures/8e507f16-4943-4be9-b707-59bd38d56309.jpg",
      text: "Đảo",
    },
    {
      img: "https://a0.muscache.com/pictures/10ce1091-c854-40f3-a2fb-defc2995bcaf.jpg",
      text: "Bãi biển",
    },
    {
      img: "https://a0.muscache.com/pictures/35919456-df89-4024-ad50-5fcb7a472df9.jpg",
      text: "Nhà nhỏ",
    },
    {
      img: "https://a0.muscache.com/pictures/50861fca-582c-4bcc-89d3-857fb7ca6528.jpg",
      text: "Thiết kế",
    },
    {
      img: "https://a0.muscache.com/pictures/8b44f770-7156-4c7b-b4d3-d92549c8652f.jpg",
      text: "Bắc cực",
    },
    {
      img: "https://a0.muscache.com/pictures/732edad8-3ae0-49a8-a451-29a8010dcc0c.jpg",
      text: "Cabin",
    },
    {
      img: "https://a0.muscache.com/pictures/677a041d-7264-4c45-bb72-52bff21eb6e8.jpg",
      text: "Ven hồ",
    },
    {
      img: "https://a0.muscache.com/pictures/6b639c8d-cf9b-41fb-91a0-91af9d7677cc.jpg",
      text: "Chơi golf",
    },
    {
      img: "https://a0.muscache.com/pictures/3b1eb541-46d9-4bef-abc4-c37d77e3c21b.jpg",
      text: "Khung cảnh tuyệt vời",
    },
    {
      img: "https://a0.muscache.com/pictures/4221e293-4770-4ea8-a4fa-9972158d4004.jpg",
      text: "Hang động",
    },
    {
      img: "https://a0.muscache.com/pictures/957f8022-dfd7-426c-99fd-77ed792f6d7a.jpg",
      text: "Lướt sóng",
    },
    {
      img: "https://a0.muscache.com/pictures/1d477273-96d6-4819-9bda-9085f809dad3.jpg",
      text: "Khung nhà chữ A",
    },
    {
      img: "https://a0.muscache.com/pictures/d7445031-62c4-46d0-91c3-4f29f9790f7a.jpg",
      text: "Nhà dưới lòng đất",
    },
  ];

  const renderPrevArrow = (callback, hasPrev) => (
    <button
      onClick={callback}
      disabled={!hasPrev}
      className="slider-button left-0"
    >
      <ChevronLeftIcon className={`${hasPrev ? '' : 'text-[#eee]'}`} />
    </button>
  );

  const renderNextArrow = (callback, hasNext) => (
    <button
      onClick={callback}
      disabled={!hasNext}
      className="slider-button right-0 "
    >
      <ChevronRightIcon className={`${hasNext ? '' : 'text-[#eee]'}`} />
    </button>
  );

  return (
    <div className="container mx-auto">
      <Carousel
        showArrows={true}
        centerMode={true}
        preventMovementUntilSwipeScrollTolerance
        centerSlidePercentage={20}
        swipeable={true}
        showThumbs={false}
        showStatus={false}
        showIndicators={false}
        renderArrowPrev={renderPrevArrow}
        renderArrowNext={renderNextArrow}
      >
        {data.map(({ img, text }, index) => (
          <div key={index} className="select-none min-h-[80px] flex flex-col items-center justify-center border-transparent border-b-2 hover:border-b-slate-400">
            <img src={img} width={24} className="!w-[24px]" />
            <div>{text}</div>
          </div>
        ))}
      </Carousel>
    </div>
  );
}
