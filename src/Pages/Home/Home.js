import React from "react";
import HomeSlider from "./components/Slider";
import HomeList from "./components/HomeList";

function Home() {
  return (
    <div className="home">
      <div className="mt-4">
        <HomeSlider />
      </div>

      <div className="my-4">
        <HomeList />
      </div>
    </div>
  );
}

export default Home;
