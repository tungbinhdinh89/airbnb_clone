import React, { useState } from "react";
import "./Header.css";
import SearchIcon from "@mui/icons-material/Search";
import LanguageIcon from "@mui/icons-material/Language";
// import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import MenuIcon from "@mui/icons-material/Menu";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import Collapse from "@mui/material/Collapse";
import Button from "@mui/material/Button";

function Header() {
  const [isSearchExpanded, setSearchExpand] = useState(false);

  return (
    <div className={`border-b${isSearchExpanded ? " shadow-md" : ""}`}>
      <header className="container mx-auto mb-3">
        <div className="flex items-center">
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a href="#" className="block no-underline">
            <img
              className="h-[64px]"
              src="assets/images/airbnb-logo.png"
              alt="icon-airbnb"
              height={32}
            />
          </a>

          {!isSearchExpanded && (
            <div className="flex-1">
              <div className="flex-1 flex justify-center">
                <div
                  className="flex items-center border rounded-full gap-3 py-1 text-sm cursor-pointer"
                  onClick={() => setSearchExpand(!isSearchExpanded)}
                >
                  <div className="px-2 border-r">Any location</div>
                  <div className="px-2 border-r">Any week</div>
                  <div>Add guess</div>
                  <div className="flex items-center justify-center bg-red-500 rounded-full p-1 mr-2">
                    <SearchIcon className="text-white !text-[1rem]" />
                  </div>
                </div>
              </div>
            </div>
          )}

          {isSearchExpanded && (
            <div className="flex-1">
              <div className="flex-1 flex justify-center">
                <div
                  className="flex items-center rounded-full gap-3 py-1 text-sm cursor-pointer"
                  onClick={() => setSearchExpand(!isSearchExpanded)}
                >
                  <div className="px-2">Place</div>
                  <div className="px-2">Experience</div>
                  <div>Online Experience</div>
                </div>
              </div>
            </div>
          )}

          <div className="flex items-center gap-4">
            <div className="hover:bg-slate-300 px-2 py-[4px] rounded-full">
              Become a host
            </div>

            <div className="hover:bg-slate-300 px-2 py-[4px] rounded-full">
              <LanguageIcon />
            </div>

            {/* <ExpandMoreIcon /> */}
            <div className="border rounded-full px-2 py-1 gap-2 flex items-center hover:shadow-md">
              <MenuIcon />
              <AccountCircleIcon fontSize="large" />
            </div>
          </div>
        </div>

        <div className="flex justify-center">
          <Collapse in={isSearchExpanded} orientation="vertical">
            <div className="flex bg-slate-2002 border rounded-full search-box">
              <div className="px-3 bg-slate-300 rounded-full flex items-center">
                <label className="">Location</label>
                <select className="min-w-[200px]">
                  <option value="HCM">HCM</option>
                  <option value="Da Lat">Da Lat</option>
                </select>
              </div>

              <div className="px-4 py-2 hover:bg-slate-300 rounded-full">
                <label className="block text-slate-400">Checkin</label>
                <input type="date" />
              </div>

              <div className="px-4 py-2 hover:bg-slate-300 rounded-full">
                <label className="block">Checkout</label>
                <input type="date" />
              </div>

              <div className="px-4 py-2 hover:bg-slate-300 rounded-full flex justify-center items-center">
                <div className="mr-6">
                  <label className="block">Guess</label>
                  <input
                    placeholder="Add guess"
                    type="number"
                    className="number-text w-[100px]"
                  />
                </div>
                <Button
                  variant="contained"
                  color="error"
                  className="!rounded-full"
                >
                  Search
                </Button>
              </div>
            </div>
          </Collapse>
        </div>
      </header>
    </div>
  );
}

export default Header;
